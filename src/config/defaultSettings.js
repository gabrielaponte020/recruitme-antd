export default {
  navTheme: 'dark', // theme for nav menu
  primaryColor: '#00A24B', // primary color of ant design
  layout: 'sidemenu', // nav menu position: `sidemenu` or `topmenu`
  contentWidth: 'Fluid', // layout of content: `Fluid` or `Fixed`, only works when layout is topmenu
  fixedHeader: true, // sticky header
  fixSiderbar: true, // sticky siderbar
  colorWeak: false,
  menu: {
    locale: true,
  },
  title: 'Ant Design Pro',
  pwa: false,
  iconfontUrl: '',
  production:
    process.env.NODE_ENV === 'production' &&
    process.env.VUE_APP_PREVIEW !== 'true',
};
